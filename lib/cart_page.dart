import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:epsi_shop/product_model.dart';

import 'cart_model.dart';
import 'listview_products.dart';

class CartPage extends StatelessWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Product> product = context.watch<CartModel>().getProducts() ;
    return Scaffold(
      appBar: AppBar(title: Text("Panier EpsiShop")),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: [
              Text("Votre panier contient ${context.watch<CartModel>().getCount()} élément",
                style: Theme.of(context).textTheme.headline6,
              ),
              Expanded(
                child:  ListView.builder(
                    itemCount: context.watch<CartModel>().getCount(),
                    itemBuilder: (context, index) => ListTile(
                      onTap:(){
                        context.go("/detail",extra: product[index]);
                      } ,
                      title: Text(product[index].nom),
                      subtitle: Text(product[index].displayPriceInEuro()),
                      leading: Image.network(product[index].image,width: 80,height: 80,),
                      trailing: TextButton(
                        child: const Text("Remove"),onPressed: (){
                        context.read<CartModel>().remove(product[index]);
                      },),
                    ),
                ),
              ),
              Spacer(),
              Text("Votre panier total est de : ${context.watch<CartModel>().prixTotal()} €",
                style: Theme.of(context).textTheme.headline6,
              )
            ],
          ),
        ),
      ),
    );
  }
}
